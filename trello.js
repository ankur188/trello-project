const board_id = '5e0627e08ca48822ac5e12e3';
const list_id = '5e0628c3ca971485aa16864a';
const token =
  '7da319ed908fadf5b55ee42b11f47c77a3dc1a29a2b5adc324bc772ae100a08c';
const key = '32851db67dc0552bd9418f377e9d7ce6';

// get request for all cards on loading a page

function getAllCards() {
  fetch(
    `https://api.trello.com/1/lists/${list_id}/cards?key=${key}&token=${token}`,
    {
      method: 'GET'
    }
  )
    .then(data => data.json())
    .then(data =>
      data.forEach(cardObj => {
        let list = document.querySelector('#list');
        let cardElement = document.createElement('div');
        cardElement.classList = 'card';
        cardElement.setAttribute('card_id', cardObj.id);
        cardElement.setAttribute('card_name', cardObj.name);
        cardElement.setAttribute('data-toggle', 'modal');
        cardElement.setAttribute('data-target', `#${cardObj.id}`);
        cardElement.innerHTML = `${cardObj.name}<div class="card-buttons" card_id="${cardObj.id}">
        <span class="btn-success check">4/4</span>
        <span
          ><button class="btn btn-default btn-xsm delete-card">
            X
          </button></span
        >
      </div>`;
        list.appendChild(cardElement);
      })
    );
}
getAllCards();

// button to open add card form

let addCardButton = document.querySelector('.add-card');
addCardButton.addEventListener('click', addCardMenu);

// display a add card form when 'add a card button' is clicked

function addCardMenu(event) {
  event.target.style.display = 'none';
  let addCardForm = document.querySelector('.add-card-form');
  addCardForm.style.display = 'block';
  document.querySelector('.add-card-menu-input').focus();
}

// button to close add card form

let closeForm = document.querySelector('.button-close');
closeForm.addEventListener('click', closeAddCardForm);

// close a add card form when close button on add card form is clicked

function closeAddCardForm(event) {
  let inputValue = document.querySelector('.add-card-menu-input');
  inputValue.value = '';
  event.target.parentElement.parentElement.style.display = 'none';
  addCardButton.style.display = 'block';
}

// button to add a card to the list

let addCardForm = document.querySelector('.button-add');
addCardForm.addEventListener('click', addCardToList);

// 1. add a card to list when a add card button is clicked on a add card form
// 2. condition: card name input must have some value

function addCardToList(event) {
  let inputValue = document.querySelector('.add-card-menu-input');
  if (inputValue.value.trim() != '') {
    fetch(
      `https://api.trello.com/1/cards?name=${inputValue.value}&idList=${list_id}&keepFromSource=all&key=${key}&token=${token}`,
      {
        method: 'POST'
      }
    )
      .then(data => data.json())
      .then(data => {
        let list = document.querySelector('#list');
        let cardElement = document.createElement('div');
        cardElement.classList = 'card';
        cardElement.setAttribute('card_id', data.id);
        cardElement.setAttribute('card_name', data.name);
        cardElement.setAttribute('data-toggle', 'modal');
        cardElement.setAttribute('data-target', `#${data.id}`);
        cardElement.innerHTML = `${data.name}<div class="card-buttons">
        <span class="btn-success check">4/4</span>
        <span
          ><button class="btn btn-default btn-xsm delete-card">
            X
          </button></span
        >
      </div>`;
        list.appendChild(cardElement);
      });
  } else {
    inputValue.focus();
  }
  inputValue.value = '';
}

let list = document.querySelector('#list');
list.addEventListener('click', runFuntionsOnLIst);

//function to perform multiple functions on the list
//set id attribute on modal wrt to the card clicked
// call a function to get all the checklists

function runFuntionsOnLIst(event) {
  if (event.target.classList.value === 'btn btn-default btn-xsm delete-card') {
    deleteTheCardFromList(event);
    event.stopPropagation();
    return;
  } else if (
    event.target.classList.value === 'card' ||
    event.target.classList.value === 'card-buttons'
  ) {
    let cardId = event.target.getAttribute('card_id');
    document.querySelector('.modal').setAttribute('id', cardId);
    modifyModalArchitecture(event, cardId);
  }
}

//function to delete a card from the list

function deleteTheCardFromList(event) {
  let card_id = event.target.parentElement.parentElement.parentElement.getAttribute(
    'card_id'
  );
  fetch(`https://api.trello.com/1/cards/${card_id}?key=${key}&token=${token}`, {
    method: 'DELETE'
  }).then(() =>
    event.target.parentElement.parentElement.parentElement.remove()
  );
}

// fetch all the cards and modify the modal title and architecture wrt to card id
// then call a function which will fetch all the items and modify checklist architecture

async function modifyModalArchitecture(event, cardId) {
  document.querySelector('.modal-title').innerText = event.target.getAttribute(
    'card_name'
  );
  fetch(
    `https://api.trello.com/1/cards/${cardId}/checklists?checkItems=all&checkItem_fields=name%2CnameData%2Cpos%2Cstate&filter=all&fields=all&key=${key}&token=${token}`,
    {
      method: 'GET'
    }
  )
    .then(data => data.json())
    .then(async data => {
      let check_List = document.querySelector('.check-list');
      check_List.innerHTML = '';
      await data.forEach(async checkListObj => {
        let checkListElement = document.createElement('div');
        checkListElement.classList = 'check-list-added';
        checkListElement.innerHTML = `<div id="${checkListObj.id}">
      <h5>${checkListObj.name}</h5>
      <div class="check-list-items"></div>
      <div class="check-list-buttons">
      <button class="btn btn-primary btn-sm add-checklist-item">
        Add an item
      </button>
      <button class="btn btn-danger btn-xsm delete-checklist">X</button>
      </div>
      <div class="add-check-list-item-form"><input
      type="text"
      placeholder="Enter title for this checklist item"
      class="add-check-list-item-input"
    /> <div><button class="add-item-to-checklist btn btn-primary btn-xsm">Add item</button>
    <button class="btn btn-danger btn-xsm cancel-add-item-form">X</button></div></div>
    </div>`;
        check_List.appendChild(checkListElement);
        await modifyCheckListArchitecture(checkListObj.id, cardId);
      });
    });
}

// function to get all check items and modify architecture of checklist

async function modifyCheckListArchitecture(checkListId, cardId) {
  await fetch(
    `https://api.trello.com/1/checklists/${checkListId}/checkItems?key=${key}&token=${token}`,
    {
      method: 'GET'
    }
  )
    .then(data => data.json())
    .then(data => {
      data.forEach(checkItems => {
        let checkListDiv = document.getElementById(checkListId).childNodes[3];
        let checklistitem = document.createElement('div');
        checklistitem.classList = 'items';
        checklistitem.setAttribute('id', checkItems.id);
        checklistitem.setAttribute('card_id', cardId);
        checklistitem.innerHTML = `<input
        type="checkbox"
        class="check-box"
      /><p>${checkItems.name}</p><button class="btn btn-default btn-xsm pull-right delete-item">X</button>
      `;
        if (checkItems.state === 'complete')
          checklistitem.firstChild.checked = true;
        checkListDiv.appendChild(checklistitem);
      });
    });
}

// button to open a create checklist menu

let createCheckListButton = document.querySelector('.create-checklist');
createCheckListButton.addEventListener('click', openCheckListMenu);

// function to open a  create checkList menu

function openCheckListMenu(event) {
  event.target.style.display = 'none';
  document.querySelector('.add-check-list-form').style.display = 'block';
  document.querySelector('.add-check-list-menu-input').focus();
}

// button to close create checkList menu

let closeCheckListButton = document.querySelector('#button-close-checklist');
closeCheckListButton.addEventListener('click', closeCheckListMenu);

// function to close checkList menu

function closeCheckListMenu(event) {
  event.target.parentElement.parentElement.style.display = 'none';
  createCheckListButton.style.display = 'block';
}

// button to add the checklist

let addCheckListButton = document.querySelector('.button-add-checklist');
addCheckListButton.addEventListener('click', addTheCheckList);

// function to add the checklist

function addTheCheckList(event) {
  event.target.parentElement.parentElement.style.display = 'none';
  let card_id = event.target.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.getAttribute(
    'id'
  ); // get card id from parent modal
  let inputValueForCheckList = document.querySelector(
    '.add-check-list-menu-input'
  );
  if (inputValueForCheckList.value.trim() != '') {
    fetch(
      `https://api.trello.com/1/checklists?idCard=${card_id}&name=${inputValueForCheckList.value}&key=${key}&token=${token}`,
      {
        method: 'POST'
      }
    )
      .then(data => data.json())
      .then(data => {
        let checkListElement = document.createElement('div');
        checkListElement.innerHTML = `<div id=${data.id}>
        <h5>${data.name}</h5>
        <div class="check-list-items"></div>
        <div class="check-list-buttons">
        <button class="btn btn-primary btn-sm add-checklist-item">
          Add an item
        </button>
        <button class="btn btn-danger btn-xsm delete-checklist">X</button>
        </div>
        <div class="add-check-list-item-form"><input
      type="text"
      placeholder="Enter title for this checklist item"
      class="add-check-list-item-input"
    /> <div><button class="add-item-to-checklist btn btn-primary btn-xsm">Add item</button>
    <button class="btn btn-danger btn-xsm cancel-add-item-form">X</button></div></div>
      </div>`;
        document.querySelector('.check-list').appendChild(checkListElement);
      });
  } else {
    inputValueForCheckList.focus();
  }
  inputValueForCheckList.value = '';

  createCheckListButton.style.display = 'block';
}

// add a event listener on whole check list

let completeCheckList = document.querySelector('.check-list');
completeCheckList.addEventListener(
  'click',
  callDifferentFunctionsForCheckListAndItems
);

// button to delete the checklist
// call function to delete the check list
// button to open add checklist item form
//call function to add a checklist item form
//button to close add checklist item form
// call a function to close a add checklist item form
// button to add an item to  the checklist
// call function to add an item to the checklist
// button to delete the item from the checklist
// call a function to delete the checklist item
// button to update check box of item
// call a function to make update to checkbox

function callDifferentFunctionsForCheckListAndItems(event) {
  if (
    event.target.classList.value === 'btn btn-danger btn-xsm delete-checklist'
  ) {
    deleteCheckList(event);
  } else if (
    event.target.classList.value === 'btn btn-primary btn-sm add-checklist-item'
  ) {
    openAddCheckListItemForm(event);
  } else if (
    event.target.classList.value ===
    'btn btn-danger btn-xsm cancel-add-item-form'
  ) {
    closeAddCheckListItemForm(event);
  } else if (
    event.target.classList.value ===
    'add-item-to-checklist btn btn-primary btn-xsm'
  ) {
    addCheckListItemToCheckList(event);
  } else if (
    event.target.classList.value ===
    'btn btn-default btn-xsm pull-right delete-item'
  ) {
    deleteCheckListItem(event);
  } else if (event.target.classList.value === 'check-box') {
    updateTheCheckBoxStatus(event);
  }
}

// function to delete a checklist

function deleteCheckList(event) {
  checkListId = event.target.parentElement.parentElement.getAttribute('id');
  fetch(
    `https://api.trello.com/1/checklists/${checkListId}?key=${key}&token=${token}`,
    {
      method: 'DELETE'
    }
  ).then(() => event.target.parentElement.parentElement.parentElement.remove());
}

// function to open a add check list item form

function openAddCheckListItemForm(event) {
  event.target.parentElement.parentElement.childNodes[5].style.display = 'none';
  event.target.parentElement.parentElement.childNodes[7].style.display =
    'block';
  document.querySelector('.add-check-list-item-input').focus();
}

// function to close add checklist item form

function closeAddCheckListItemForm(event) {
  event.target.parentElement.parentElement.parentElement.childNodes[5].style.display =
    'block';
  event.target.parentElement.parentElement.style.display = 'none';
}

// function to delete a checklist item

function deleteCheckListItem(event) {
  let checkItemId = event.target.parentElement.getAttribute('id');
  let checkListId = event.target.parentElement.parentElement.parentElement.getAttribute(
    'id'
  );
  fetch(
    `https://api.trello.com/1/checklists/${checkListId}/checkItems/${checkItemId}?key=${key}&token=${token}`,
    {
      method: 'DELETE'
    }
  ).then(() => event.target.parentElement.remove());
}

// function to make update to checkbox of checklist item

function updateTheCheckBoxStatus(event) {
  let state = 'incomplete';
  if (event.target.checked === true) {
    state = 'complete';
  }
  console.log(state);
  fetch(
    `https://api.trello.com/1/cards/${event.target.parentElement.getAttribute(
      'card_id'
    )}/checkItem/${event.target.parentElement.getAttribute(
      'id'
    )}?state=${state}&key=${key}&token=${token}`,
    {
      method: 'PUT'
    }
  );
}

// function to add an item to the checklist

function addCheckListItemToCheckList(event) {
  let checklist_id = event.target.parentElement.parentElement.parentElement.getAttribute(
    'id'
  );
  let checklistElement = document.getElementById(checklist_id);
  let carId = checklistElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.getAttribute(
    'id'
  );

  // console.log(checklistElement);
  let checklistItemName = event.target.parentElement.parentElement.firstChild;

  if (checklistItemName.value.trim() != '') {
    fetch(
      `https://api.trello.com/1/checklists/${checklist_id}/checkItems?name=${checklistItemName.value}&pos=bottom&checked=false&key=${key}&token=${token}`,
      {
        method: 'POST'
      }
    )
      .then(data => data.json())
      .then(data => {
        let itemForm = document.querySelector('.add-check-list-item-form');
        itemForm.style.display = 'none';
        document.querySelector('.check-list-buttons').style.display = 'block';
        let checkListDiv =
          event.target.parentElement.parentElement.parentElement.childNodes[3];
        let checklistitem = document.createElement('div');
        checklistitem.classList = 'items';
        checklistitem.setAttribute('id', data.id);
        checklistitem.setAttribute('card_id', carId);
        checklistitem.innerHTML = `<input
        type="checkbox"
        class="check-box"
      /><p>${data.name}</p><button class="btn btn-default btn-xsm pull-right delete-item">X</button>
      `;
        checkListDiv.appendChild(checklistitem);
      });
  } else {
    checklistItemName.focus();
  }
  checklistItemName.value = '';
}
